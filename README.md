##Das InfoSystem besteht aus einer Umlaufenden 128x8 WS2812b RGB LED Matrix


##Anzeige Content  ##

**1. Information über aktuelle Projekte**

*     Prio 5
 
     da Status des Backends unklar (micha fragen))

*     Beschreibung

     Anzeige der aktuellen Projektdaten die momentan im Hacklabor stattfinden

*     Contra
     
     die Anzeige der Matrix ist beschränkt  
   
*     Daten

    https://gitlab.com/hacklabor/Projekt-Projektliste.git
   
*     Extras / moegliche Workarrounds
    
    * mini QR Code
    * BT becaon




**2. IT News + Szene News**


*     Prio 3

*     Beschreibung
    
    Darstellung von News feed 
    Bitcoin Kursen
     
*     Contra
     die anzeige der matrix ist beschraenkt
   
*     Daten

    News feed
   

**3. als Beleuchtungsquelle in Kombi mit Uhrzeit** 
 

*     Prio 0
       
    Startprojekt simpel

*     Beschreibung
    
    Die Anzeige fungiert als Lampe welche verschiedene Farbverläufe und Lichtstimmungen erzeugen kann. Optional kann auch noch die aktuelle Uhrzeit mit eingeblendet werden. entweder als _Kontrastfarbe_ oder _Schwarz_ (LED aus) 
     
*     Contra
     
     Viel Energie für wenig Lichtausbeute
   
*     Daten

    NodeRed 
    
    Lampe: (wie für die Flauschecke), Lichtstimmungen, Hellgkeitssensor
    
    Uhr: Farbe,Position,Kopien (1x,2x etc.), Abstand der Kopien, statisch, bewegt, speed, Richtung 
  
    

**4. Status bei Aufzeichnungen/Streaming.**

*     Prio 7
       
     selten im Einsatz

*     Beschreibung
    
    Jeder im Raum kann den aktuellen Status der Aufzeichnung sehen z.B. Mikro ist offen, Es wird aufgezeichnet, Es wird gestreamt etc.
     
*     Contra
     
     n.a.
   
*     Daten

    Auf Streaming Rechner Script via MQTT Daten aus OBS
    Daten Umlaufend bzw Statische Szenen
    

 
**Timerfunktion** 

*     Prio 2
       
     simpel

*     Beschreibung
    
    Timerfunktion als Stoppuhr und Countdown
    z.B für Vorträge als Speaker Timer
     
*     Contra
     
     setzt den Speaker unter Druck, wenn dann sehr ungenau ... 50%,  15min,  10min, 5min, 2min, <1 min 

   
*     Daten

    von Nodered
    
    Start/Stop,Startzeit für Countdown, Position, Kopien (1x, 2x,...), Abstand der Kopien,bewegt oder statisch, Spezieller Speacker Timer für Vorträge siehe Contra 
  




**7. Freitext**

*     Prio 2
       
     simpel

*     Beschreibung
    
    Daten wie Offen, Energieverbrauch, Chuck etc. oder eigener Text kann hier Dargestellt werden
     
*     Contra
     


   
*     Daten

    von Nodered
    
    String, Farbe, Position, Speed, Richtung,umlaufend
     
  

**8. lustige  Animationen (Sprites 8x8, 8x16, 8x32 etc.)**

*     Prio
       
     6 arbeitsintensiv

*     Beschreibung
    
    Es können verschiedene Animationen welche auf dem Pi hinterlegt sind abgespielt werden. z.B Pac Man als wir löschen alles. etc.
     
*     Contra
     


   
*     Daten

    Nodred 
    als Triger Byte 
    Animation selber Direkt auf PI



**9. Status der TGZ PokemonGo Arena =)** Prio 8

**10. Ich würde einen Modi machen, wo zb Uhr und Space Status rund herum angezeigt werde...**

 Siehe Freitext Funktion


----

Fazit 
Mit der Freitextfunktion in Verbindung der Lampenfunktion als Hintergrundfarbe kann man schon vieles Darstellen. Und andere Funktionen wie die Uhr können das Nutzen


----

     
    

Hardware 

1. 4x 8x32 RGB Panels (vorhanden)
2. 2x Netzteil a 200W (bestellt)
3 NANO oder STM32 beide vorhanden
4 PI 2 oder 3  vielleicht reicht auch ein Zero


----


Software PI

- MQTT client
- SSH 
- Python als Steuerprogramm
- Scenen Editor als GUI bzw Ablage von 128x8 Bitmaps 24bit Farbtiefe
- https://github.com/jgarff/rpi_ws281x
- nodeRed als Programieroberfläche?


----

* Start des Pi's über wake on lan bei Status offen oder Handtrigger
* Shutdown über MQTT befehl
* eigenes Netzteil erforderlich. 
* oder eigene Steckdose schaltbar. dann Kein eigenes Netzteil

